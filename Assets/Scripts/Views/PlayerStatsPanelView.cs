﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsPanelView : MonoBehaviour
{

    [SerializeField]
    private Text m_nextWaveText = null;

    [SerializeField]
    private Text m_nextWaveLabelText = null;

    [SerializeField]
    private Text m_scoreText = null;

    [SerializeField]
    private Text m_fundsText = null;

    [SerializeField]
    private Text m_coreHealthText = null;


    public void Init(PlayerData playerData)
    {
        UpdateScore(playerData.Score);
        UpdateFunds(playerData.Funds);
        UpdateCore(playerData.CoreHealth);

        GameController.Instance.NextWaveIn += UpdateNextWaveIndication;

        playerData.OnScoreChanged += UpdateScore;
        playerData.OnFundsChanged += UpdateFunds;
        playerData.OnCoreChanged += UpdateCore;
    }

    private void UpdateNextWaveIndication(int nextWaveTime)
    {
        m_nextWaveText.text = nextWaveTime.ToString();
    }

    private void UpdateScore(int score)
    {
        m_scoreText.text = score.ToString();
    }

    private void UpdateFunds(int funds)
    {
        m_fundsText.text = funds.ToString();
    }

    private void UpdateCore(int core)
    {
        m_coreHealthText.text = core.ToString();
    }
    
    public void ShowHideNextWaveIndication(bool show)
    {
        m_nextWaveText.gameObject.SetActive(show);
        m_nextWaveLabelText.gameObject.SetActive(show);
    }
}
