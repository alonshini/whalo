﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//holds all the references to items in the scene
public class ManagerView : MonoBehaviour
{
    public static ManagerView Instance;

    [SerializeField]GameObject m_airTowerPrefab;
    [SerializeField] GameObject m_groundTowerPrefab;
    [SerializeField] GameObject m_airUnitPrefab;
    [SerializeField] GameObject m_groundUnitPrefab;
    [SerializeField] GameObject m_towerPivotPrefab;


    [SerializeField] Transform m_unitsHolder;
    [SerializeField] Transform m_pivotsHolder;


    private void Awake()
    {
        Instance = this;
    }


    public GameObject AirTowerPrefab { get => m_airTowerPrefab;}
    public GameObject GroundTowerPrefab { get => m_groundTowerPrefab;}
    public GameObject AirUnitPrefab { get => m_airUnitPrefab;}
    public GameObject GroundUnitPrefab { get => m_groundUnitPrefab;}
    public GameObject TowerPivotPrefab { get => m_towerPivotPrefab; }

    public Transform UnitsHolder { get => m_unitsHolder; }
    public Transform PivotsHolder { get => m_pivotsHolder;}
}
