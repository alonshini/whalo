﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Tower;

public class TowerView : MonoBehaviour
{
    public enum TowerState
    {
        looking,
        moving,
        shooting,
    }

    [SerializeField] ModelManager.TowerType m_towerType;


    [SerializeField] Transform m_torretHead;
    [SerializeField] GameObject m_towerProjectile;
    [SerializeField] Transform m_raySource;
    [SerializeField] int m_towerLayerMask;
    [SerializeField] int m_enemyLayerMask;
    [SerializeField] float m_rotateSpeed = 100;
    [SerializeField] float m_shotInterval = 0.25f;
    [SerializeField] float m_projectileSpeed;
    float m_towerRange = 0;
    int m_towerCost = 0;

    TowerState m_towerState = TowerState.looking;

    float m_moveToAngle = 0;
    float m_deltaAngle = 0;

    float m_totalAddedToAngle = 0;
    UnitView m_selectedTarget = null;

    float m_shotIntervalCounter = 0;

    List<UnitView> m_inRange = new List<UnitView>();

    private void Awake()
    {
        m_towerRange = gameObject.GetComponent<SphereCollider>().radius;

        if (m_towerType == ModelManager.TowerType.air)
            m_towerCost = GameController.AIR_BLDG_COST;
        else
            m_towerCost = GameController.GROUND_BLDG_COST;
    }

    private void Update()
    {
        if (m_towerState == TowerState.looking)
        {
            if (m_inRange.Count > 0)
                PickTarget();


        }/*
        else if (_torretState == TorretState.moving)
        {
            float addToAngle = Time.deltaTime * _rotateSpeed * _deltaAngle / 100f;

            _totalAddedToAngle += Mathf.Abs(addToAngle);

            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + addToAngle, transform.localEulerAngles.z);

            if (_totalAddedToAngle > Mathf.Abs(_deltaAngle))
                _torretState = TorretState.shooting;


        }*/
        else if (m_towerState == TowerState.shooting)
        {
            bool shot = false;
            m_shotIntervalCounter += Time.deltaTime;

            if (m_shotIntervalCounter > m_shotInterval)
            {
                shot = true;
                m_shotIntervalCounter -= m_shotInterval;

            }
            //need to make sure the target i was aiming at is still alive

            if (m_selectedTarget != null)
            {
                if (shot)
                    ShotProjectile();

            }
            else
            {
                m_towerState = TowerState.looking;
            }

        }

    }

    private void ShotProjectile()
    {
        Debug.Log("Projectile Shot");
        GameObject projectile = Instantiate(m_towerProjectile);
        projectile.transform.position = m_raySource.position;
        projectile.transform.LookAt(m_selectedTarget.transform.position);
        projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * m_projectileSpeed;
        m_towerState = TowerState.looking;
    }

    private void PickTarget()
    {

        //remove dead units that died while in the kill zone - so they never left the trigger and were never removed
        for (int i = m_inRange.Count - 1; i >= 0; i--)
        {
            if (m_inRange[i] == null)
                m_inRange.RemoveAt(i);
        }

        //arrange list by ditances
        bool done = false;

        while (!done)
        {
            done = true;
            for (int i = 0; i < m_inRange.Count - 2; i++)
            {
                if (GetDistance(m_inRange[i]) > GetDistance(m_inRange[i + 1]))
                {
                    UnitView tempUnitView = m_inRange[i];
                    m_inRange[i] = m_inRange[i + 1];
                    m_inRange[i + 1] = tempUnitView;
                    done = false;
                }
            }
        }

        //start going over the sorted list - see if item is visible - and if so pick it and start moving to it.
        m_selectedTarget = null;

        bool itemFound = false;

        foreach (UnitView item in m_inRange)
        {
            if (!itemFound)
            {
                if (IsItemInSight(item))
                {
                    itemFound = true;
                    Vector3 P1 = m_raySource.position;
                    Vector3 P2 = item.RayTarget.position;

                    Vector3 targetDir = P2 - P1;
                    m_moveToAngle = Vector3.Angle(targetDir, transform.forward);

                    m_deltaAngle = m_moveToAngle - transform.localEulerAngles.y;

                    m_totalAddedToAngle = 0;

                    m_selectedTarget = item;

                    //override as i had issues with the tower head rotation - so i snap to target instead of move
                    m_torretHead.eulerAngles = new Vector3(m_torretHead.eulerAngles.x, m_moveToAngle, m_torretHead.eulerAngles.z);
                    

                    m_towerState = TowerState.shooting;
                }
            }

        }

    }
    //looks for potential targets that are inside the kill zone - 
    // filters out other tower hits
    private bool IsItemInSight(UnitView item)
    {
        Vector3 P1 = m_raySource.position;
        Vector3 P2 = item.RayTarget.position - m_raySource.position;

        RaycastHit hit;
        Ray ray = new Ray(P1, P2);

        int layerMask = 1 << m_enemyLayerMask;//GroundEnemy Layer
        int layerMask2 = 1 << m_towerLayerMask;//Torrets Layer

        LayerMask combinedMask = layerMask | layerMask2;
        UnitView targetUnitView = null;

        Debug.DrawRay(m_raySource.position, (item.RayTarget.position - m_raySource.position), Color.red);
        if (Physics.Raycast(ray, out hit, m_towerRange, combinedMask))
        {
            //filters out hitting other towers
            if (hit.transform.gameObject.layer != m_towerLayerMask)
            {
                targetUnitView = hit.transform.gameObject.GetComponent<UnitView>();

                //making sure i only shot at targets that are alive (the start indicates that)
                if (item == targetUnitView && targetUnitView.Start)
                    return true;
                else
                    return false;
            }
            else
                return false;

        }
        else
            return false;
    }


    //util function to measure distance - to pick the correct viable target unit
    private float GetDistance(UnitView baseUnitView)
    {
        if (baseUnitView == null)
            return 1000;
        else
            return Vector3.Distance(transform.position, baseUnitView.transform.position);
    }


    //marking any unit that is matching my tower type - so i only try to "look" at them
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == m_enemyLayerMask)
            m_inRange.Add(other.gameObject.GetComponent<UnitView>());
    }

    //removing them when and if they leave the kill zone
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == m_enemyLayerMask)
            m_inRange.Remove(other.gameObject.GetComponent<UnitView>());
    }

    public bool IsPlaced { get; protected set; }
    public int TowerCost { get => m_towerCost; }

    public void SetParentPivot(Transform pivotTransform)
    {
        transform.SetParent(pivotTransform);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

    public virtual void OnPlacement() { IsPlaced = true; }

    public virtual void OnRemoval() { Destroy(gameObject); }
}
