﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBuildingPivotView : MonoBehaviour
{
    [SerializeField]
    private GameObject m_placementIndicator = null;

    public TowerView CurrentTower { get; private set; }

    public Quaternion Rotation => transform.rotation;
    public Vector3 WorldPosition => transform.position;

    public void SetPosition(Vector3 position)
    {

        transform.position = position;
    }

    public void SetBuilding(TowerView tower)
    {
        if (CurrentTower != null)
        {
            Debug.LogError("Current building already set, cannot add another building to pivot", gameObject);
            return;
        }
        m_placementIndicator.SetActive(false);
        CurrentTower = tower;
        CurrentTower.SetParentPivot(transform);
        CurrentTower.transform.localScale = new Vector3(1f, 1f, 1f);
        CurrentTower.OnPlacement();
    }

    public void RemoveTower()
    {
        if (CurrentTower == null)
        {
            Debug.LogError("There is no current tower to remove on this pivot", gameObject);
            return;
        }
        var tower = CurrentTower;
        CurrentTower = null;
        tower.OnRemoval();
        m_placementIndicator.SetActive(true);
    }

    private void OnDestroy()
    {
        CurrentTower = null;
    }
}
