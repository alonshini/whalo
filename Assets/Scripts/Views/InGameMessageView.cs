﻿using UnityEngine;
using UnityEngine.UI;

public class InGameMessageView : MonoBehaviour
{
    [SerializeField]
    private Text m_text;

    [SerializeField]
    private CanvasGroup m_canvasGroup = null;

    [SerializeField]
    private AnimationCurve m_alphaCurve = AnimationCurve.EaseInOut(0, 1, 1, 0);

    private float m_remainingShowTime = 0;
    private float m_totalShowTime = 0;

    public void Show(string text, float duration)
    {
        m_text.text = text;
        m_remainingShowTime = duration;
        m_totalShowTime = duration;
        gameObject.SetActive(true);
    }

    public void UpdateText(string text)
    {
        m_text.text = text;
    }

    private void Update()
    {
        m_remainingShowTime -= Time.deltaTime;
        m_canvasGroup.alpha = m_alphaCurve.Evaluate(1 - (m_remainingShowTime / m_totalShowTime)); 
        if (m_remainingShowTime <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
