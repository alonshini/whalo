﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UIController;

public class TowerMenuView : MonoBehaviour
{
    [SerializeField] GameObject m_buyMenu;
    [SerializeField] GameObject m_sellMenu;
    [SerializeField] Transform m_menusHolder;

    Action<BuySellMenuAnswers> m_callback;

    public void ShowBuYSellMenu(Vector3 position,bool showBuy, Action<BuySellMenuAnswers> answer)
    {
        m_menusHolder.localPosition = position;

        if (showBuy)
            m_buyMenu.SetActive(true);
        else
            m_sellMenu.SetActive(true);

        m_callback = answer;
    }

    public void BuyGroundClicked()
    {
        m_callback(BuySellMenuAnswers.buyGround);
        CloseMenu();
    }

    public void BuyAirClicked()
    {
        m_callback(BuySellMenuAnswers.buyAir);
        CloseMenu();
    }

    public void SellClicked()
    {
        m_callback(BuySellMenuAnswers.sell);
        CloseMenu();
    }

    public void CloseClicked()
    {
        m_callback(BuySellMenuAnswers.cancel);
        CloseMenu();
    }

    private void CloseMenu()
    {
        m_buyMenu.SetActive(false);
        m_sellMenu.SetActive(false);
        gameObject.SetActive(false);
    }
}
