﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitView : MonoBehaviour
{
    Unit m_model;

    [SerializeField] Transform m_rayTarget;
    
    Vector3 m_target;
    Vector3 m_nextTarget;
    Vector3 m_direction = new Vector3();
    float m_timeCounter = 0;
    float m_distance = 0;
    float m_totalTime = 0;
    int m_currPathIndex = 0;

    [SerializeField] int m_damageToCore;
    [SerializeField] int _projectileDamage;
    [SerializeField] int m_layerMask;
    [SerializeField] int m_projectileLayerMask;
    [SerializeField] GameObject m_unitDieEffect;
    GameObject m_mainHolder;

    bool m_start = false;

    Vector3[] _path;

    
    public Transform RayTarget { get => m_rayTarget; }
    public Unit Model { get => m_model;}
    public int DamageToCore { get => m_damageToCore;}
    public bool Start { get => m_start;}

    public void Init(Unit model)
    {
        m_mainHolder = transform.GetChild(0).gameObject;
        m_model = model;
        m_currPathIndex = 0;
        transform.position = m_model.UnitPath[m_currPathIndex];
        SetNextPathPoint();
    }



    private void SetNextPathPoint()
    {
        if (m_currPathIndex < m_model.UnitPath.Length - 1)
        {
            m_target = m_model.UnitPath[m_currPathIndex];
            m_currPathIndex++;
            m_nextTarget = m_model.UnitPath[m_currPathIndex];

            //rotates the unit in the path direction
            transform.LookAt(m_nextTarget);
            m_direction = new Vector3();
            m_direction = m_nextTarget - m_target;
            //the distance between the vector can be used to measure the distance travelled on path points
            m_distance = m_direction.magnitude;
            //normalized so the speed will be a factor of the unit speed and not related to the distance between any 2 points
            m_direction.Normalize();
            m_totalTime = 0;

            m_start = true;
        }
        else
        {
            //core reached
            GameController.Instance.UnitReachedCore(this);
            DestroyUnit();
        }
    }


    protected virtual void Update()
    {
        if (m_start)
        {
            m_totalTime += Time.deltaTime * m_model.MoveSpeed;
            transform.localPosition += m_direction * Time.deltaTime * m_model.MoveSpeed/100f;

            if (m_totalTime >= m_distance)
                SetNextPathPoint();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == m_projectileLayerMask)
        {
            if (other.gameObject != null)
                Destroy(other.gameObject);

            m_model.Health -= _projectileDamage;

            if (m_model.Health <= 0)
            {
                GameController.Instance.UnitDied(this);
                DestroyUnit();
            }
        }
    }

    protected void DestroyUnit()
    {
        m_start = false;
        //show effect
        m_mainHolder.SetActive(false);
        m_unitDieEffect.SetActive(true);
        
        Invoke("ActualDestory", 1f);
    }

    private void ActualDestory()
    {
        if(gameObject!=null)
            Destroy(gameObject);
    }

}
