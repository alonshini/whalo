﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData 
{
    int m_score;
    int m_funds;
    int m_coreHealth;

    public Action<int> OnScoreChanged;
    public Action<int> OnFundsChanged;
    public Action<int> OnCoreChanged;

    public int Funds { get => m_funds;}
    public int Score { get => m_score; }
    public int CoreHealth { get => m_coreHealth;}

    public PlayerData(int score, int funds, int coreHealth)
    {
        m_score = score;
        m_funds = funds;
        m_coreHealth = coreHealth;
    }

    public void UpdateScore(int inc)
    {
        m_score += inc;
        OnScoreChanged?.Invoke(m_score);
    }

    public void UpdateFunds(int inc)
    {
        m_funds += inc;
        OnFundsChanged?.Invoke(m_funds);
    }

    public void UpdateCore(int inc)
    {
        m_coreHealth += inc;
        OnCoreChanged?.Invoke(m_coreHealth);
    }
}
