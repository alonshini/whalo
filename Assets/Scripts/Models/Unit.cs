﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ModelManager;

public class Unit 
{

    UnitType m_unitType;
    int m_health;
    float m_moveSPeed;
    Vector3[] m_unitPath;
    
    public Unit(int health, float moveSpeed,Vector3[] unitPath, UnitType unitType)
    {
        m_health = health;
        m_moveSPeed = moveSpeed;
        m_unitPath = unitPath;
        m_unitType = unitType;
    }

    public UnitType UnitType { get => m_unitType; }
    public float MoveSpeed { get => m_moveSPeed; }
    public Vector3[] UnitPath { get => m_unitPath; }
    public int Health { get => m_health; set => m_health = value; }
}
