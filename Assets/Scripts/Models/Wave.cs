﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave
{
    List<Unit> m_airUnits = new List<Unit>();
    List<Unit> m_groundUnits = new List<Unit>();

    public Wave(int numAir, int airSpeed, int airHealth,Vector3[] airPath, int numGround, int groundSpeed,Vector3[] groundPath, int groundHealth)
    {
        for (int i = 0; i < numAir; i++)
            m_airUnits.Add(new Unit(airHealth, airSpeed,airPath, ModelManager.UnitType.air));

        for (int i = 0; i < numGround; i++)
            m_groundUnits.Add(new Unit(groundHealth, groundSpeed,groundPath, ModelManager.UnitType.ground));

    }

    public List<Unit> AirUnits { get => m_airUnits;}
    public List<Unit> GroundUnits { get => m_groundUnits;}
}
