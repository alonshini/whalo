﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//holds  model data of the game
public class ModelManager : MonoBehaviour
{
    const int INITIAL_FUNDS = 100;
    const int INITIAL_CORE_HEALTH = 200;

    const int GROUND_UNITS_START_NUM = 8;
    const int GROUND_UNITS_START_SPEED = 1;
    const int GROUND_UNITS_START_HEALTH = 30;
    const int GROUND_UNITS_START_HEALTH_INCREASE = 20;

    const int AIR_UNITS_START_NUM = 3;
    const int AIR_UNITS_START_SPEED = 2;
    const int AIR_UNITS_START_HEALTH = 200;

    public enum UnitType
    {
        ground,
        air
    }

    public enum TowerType
    {
        ground,
        air
    }
    
    public static ModelManager Instance;
    [SerializeField] Board m_board;
    PlayerData m_playerData;


    private void Awake()
    {
        Instance = this;
    }
    
    public void ResetPlayerData()
    {
        m_playerData = new PlayerData(0, INITIAL_FUNDS, INITIAL_CORE_HEALTH);
    }
    

    public Wave GetWave(int index)
    {
        int numAirUnits = (index - 2) * AIR_UNITS_START_NUM;
        numAirUnits = Mathf.Max(0, numAirUnits);

        Wave wave = new Wave(numAirUnits, AIR_UNITS_START_SPEED, AIR_UNITS_START_HEALTH,m_board.GetAirPath(),
                GROUND_UNITS_START_NUM * index, GROUND_UNITS_START_SPEED * index,m_board.GetGroundPath(),
                GROUND_UNITS_START_HEALTH + (GROUND_UNITS_START_HEALTH_INCREASE * index));

        return wave;
    }

    public Board Board { get => m_board;}
    public PlayerData PlayerData { get => m_playerData; set => m_playerData = value; }
}
