﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public const int GROUND_BLDG_COST = 100;
    public const int AIR_BLDG_COST = 250;

    const int WAVE_INTERVAL = 10;
    const float IN_WAVE_INTERVAL = 1f;
    const int SCORE_FUNDS_PER_SECOND = 10;

    const int GROUND_UNIT_SCORE_GIVE = 10;
    const int GROUND_UNIT_FUNDS_GIVE = 10;
    const int AIR_UNIT_SCORE_GIVE = 10;
    const int AIR_UNIT_FUNDS_GIVE = 10;

    public static GameController Instance;

    bool m_isInGame = false;
    Wave m_currWave;
    int m_currWaveIndex = 1;
    
    //holds the wave unit so i know when to start the next wave
    List<UnitView> m_currWaveUnits = new List<UnitView>();
    [SerializeField] PlayerCoreView m_playerCore;

    float m_nextWaveIn = 0;
    float m_nextWaveItemIn = 0;

    //using a float so i will not lose parts of seconds
    float m_score = 0;

    //used to show the next wave counter
    public Action<int> NextWaveIn;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Debug.Log("GameController Started");
        Physics.queriesHitTriggers = true;
        UIController.Instance.ShowHideMainMenu(true);

    }

    public void StartClicked()
    {
        ResetParams();
        m_isInGame = true;
    }

    public void ExitClicked()
    {
        GameOver();
    }

    private void ResetParams()
    {
        GenerateBuildingPivots();
        ModelManager.Instance.ResetPlayerData();
        UIController.Instance.SetPlayerStatsPanel();
        m_currWave = null;
        m_currWaveIndex = 1;
        m_currWaveUnits.Clear();
        m_score = 0;
        m_nextWaveIn = WAVE_INTERVAL;
        m_nextWaveItemIn = IN_WAVE_INTERVAL;
        UIController.Instance.ShowHideNextWaveIndication(true);
    }

    private void Update()
    {
        if (m_isInGame)
        {
            float deltaTime = Time.deltaTime;

            m_score += deltaTime;

            if (m_score > 1)
            {
                m_score -= 1f;
                ModelManager.Instance.PlayerData.UpdateScore(SCORE_FUNDS_PER_SECOND);
                ModelManager.Instance.PlayerData.UpdateFunds(SCORE_FUNDS_PER_SECOND);
            }

            if (m_currWave == null)
            {
                m_nextWaveIn -= deltaTime;

                NextWaveIn?.Invoke((int)m_nextWaveIn);

                if (m_nextWaveIn <= 0)
                {
                    //build new wave
                    m_nextWaveIn = WAVE_INTERVAL;
                    m_currWave =  ModelManager.Instance.GetWave (m_currWaveIndex);
                    m_currWaveIndex++;
                    m_nextWaveItemIn = IN_WAVE_INTERVAL;
                    UIController.Instance.ShowHideNextWaveIndication(false);
                    UIController.Instance.ShowAnnouncement("Wave Coming",1.25f);
                }
            }
            else
            {
                //we are during a wave - keep generating new items until wave is over
                m_nextWaveItemIn += deltaTime;


                //wasnt mentioned in the doc - but if there isnt a wave inerval all the units will go on top of each other
                if (m_nextWaveItemIn >= IN_WAVE_INTERVAL)
                {
                    GenerateWaveItems();
                    m_nextWaveItemIn = 0;
                }
            
                if (m_currWaveUnits.Count == 0)
                {
                    m_currWave = null;
                    UIController.Instance.ShowHideNextWaveIndication(true);
                }

            }
        }
    }

    //handles the tower built menu
    public void TowerLocationClicked(BaseBuildingPivotView buildingPivot)
    {
        bool buyMode = true;
        
        if (buildingPivot.CurrentTower != null)
            buyMode = false;
        
        UIController.Instance.ShowBuySellMenu(buildingPivot.transform.position, buyMode, (UIController.BuySellMenuAnswers answer) =>
        {
            if (answer == UIController.BuySellMenuAnswers.buyAir || answer == UIController.BuySellMenuAnswers.buyGround)
            {
                int towerCost = GROUND_BLDG_COST;
                if (answer == UIController.BuySellMenuAnswers.buyAir)
                    towerCost = AIR_BLDG_COST;

                if (ModelManager.Instance.PlayerData.Funds  >= towerCost)
                {
                    GameObject Tower;
                    if (answer == UIController.BuySellMenuAnswers.buyAir)
                        Tower = Instantiate(ManagerView.Instance.AirTowerPrefab);
                    else
                        Tower = Instantiate(ManagerView.Instance.GroundTowerPrefab);

                    TowerView baseTower = Tower.GetComponent<TowerView>();

                    ModelManager.Instance.PlayerData.UpdateFunds(-baseTower.TowerCost);
                    buildingPivot.SetBuilding(baseTower);
                }
                else
                {
                    UIController.Instance.ShowAnnouncement("Insufficient Funds", 1f);
                }
            }
            else if (answer == UIController.BuySellMenuAnswers.sell)
            {
                ModelManager.Instance.PlayerData.UpdateFunds(buildingPivot.CurrentTower.TowerCost / 2);
                buildingPivot.RemoveTower();
            }

        });
    }

    public void UnitReachedCore(UnitView unitView)
    {

        m_playerCore.PlayerCoreEffect();

        m_currWaveUnits.Remove(unitView);

        ModelManager.Instance.PlayerData.UpdateCore(-unitView.DamageToCore);

        if (ModelManager.Instance.PlayerData.CoreHealth <= 0)
            GameOver();
    }

    public void GameOver()
    {
        m_isInGame = false;
        UIController.Instance.ShowAnnouncement("Game Over", 1f);
        UIController.Instance.ShowScoreScreen(ModelManager.Instance.PlayerData.Score);
        DestoryOldGame();
    }

    //units report when they die so all the score\funds is managed from one location only
    public void UnitDied(UnitView unitView)
    {
        m_currWaveUnits.Remove(unitView);

        if(unitView.Model.UnitType == ModelManager.UnitType.air)
        {
            ModelManager.Instance.PlayerData.UpdateFunds(AIR_UNIT_FUNDS_GIVE);
            ModelManager.Instance.PlayerData.UpdateScore(AIR_UNIT_SCORE_GIVE);
        }
        else
        {
            ModelManager.Instance.PlayerData.UpdateFunds(GROUND_UNIT_FUNDS_GIVE);
            ModelManager.Instance.PlayerData.UpdateScore(GROUND_UNIT_SCORE_GIVE);
        }


    }

    //clean the board from old stuff
    private void DestoryOldGame()
    {
        foreach (Transform item in ManagerView.Instance.PivotsHolder)
        {
            Destroy(item.gameObject);
        }
        foreach (Transform item in ManagerView.Instance.UnitsHolder)
        {
            Destroy(item.gameObject);
        }

    }

    //gets and generates the next unit in a wave until the wave is done
    private void GenerateWaveItems()
    {
        if (m_currWave.GroundUnits.Count > 0)
        {
            GenerateUnit(m_currWave.GroundUnits[0]);
            m_currWave.GroundUnits.RemoveAt(0);
        }
        if (m_currWave.AirUnits.Count > 0)
        {
            GenerateUnit(m_currWave.AirUnits[0]);
            m_currWave.AirUnits.RemoveAt(0);
        }
    }

    //actual unit generation
    private void GenerateUnit(Unit unit)
    {
        GameObject UnitObject;

        if (unit.UnitType == ModelManager.UnitType.air)
            UnitObject = Instantiate(ManagerView.Instance.AirUnitPrefab);
        else
            UnitObject = Instantiate(ManagerView.Instance.GroundUnitPrefab);
        
        UnitObject.transform.SetParent(ManagerView.Instance.UnitsHolder);

        UnitView baseUnitView = UnitObject.GetComponent<UnitView>();
        baseUnitView.Init(unit);

        m_currWaveUnits.Add(baseUnitView);

    }

    //build the initial pivots map - which will also later hold the towers
    private void GenerateBuildingPivots()
    {
        ManagerView.Instance.PivotsHolder.localPosition = new Vector3(0, 0f, 0);
        int index = 0;
        foreach (Vector3 buildingPivot in ModelManager.Instance.Board.GetBuildingPivots())
        {
            index++;
            GameObject buildingPivotIndication = Instantiate(ManagerView.Instance.TowerPivotPrefab);
            buildingPivotIndication.name = "Pivot" + index;
            buildingPivotIndication.transform.SetParent(ManagerView.Instance.PivotsHolder);
            BaseBuildingPivotView baseBuildingPivot = buildingPivotIndication.GetComponent<BaseBuildingPivotView>();
            baseBuildingPivot.SetPosition(buildingPivot);
        }
        ManagerView.Instance.PivotsHolder.localPosition = new Vector3(0, 0.1f, 0);
    }


    public bool IsInGame { get => m_isInGame; }
}
