﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController Instance;

    private void Awake()
    {
        Instance = this;
    }

    public enum BuySellMenuAnswers
    {
        buyGround,
        buyAir,
        sell,
        cancel
    }

    [SerializeField]
    private InGameMessageView m_announcement = null;
    
    [SerializeField]
    private PlayerStatsPanelView m_playerStatePanel = null;

    [SerializeField]
    private TowerMenuView m_buySellMenu = null;

    [SerializeField]
    GameObject m_mainMenuScreen = null;

    [SerializeField]
    GameObject m_scoreScreen=null;

    //little foul here - as its directly controller from the UIController and not via another view 
    [SerializeField] Text m_scoreScreenHighScore;

    [SerializeField]
    GameObject m_scorePanel = null;

    //gives the panel view its model to refer to - so score/funds/core updates come via events
    public void SetPlayerStatsPanel()
    {
        m_playerStatePanel.Init(ModelManager.Instance.PlayerData);
    }

    //geneal message class
    public void ShowAnnouncement(string message, float time)
    {
        m_announcement.Show(message, time);
    }

    //wave inidication only active not during waves
    public void ShowHideNextWaveIndication(bool show)
    {
        m_playerStatePanel.ShowHideNextWaveIndication(show);
    }

    public void ShowHideMainMenu(bool show)
    {
        m_mainMenuScreen.SetActive(show);
    }

    public void ShowScoreScreen(int score)
    {
        //manages the high score in player prefs
        int currHighScore = PlayerPrefs.GetInt("Score", 0);

        if(score > currHighScore)
            PlayerPrefs.SetInt("Score", score);


        m_scoreScreenHighScore.text = Mathf.Max(score, currHighScore).ToString();

        m_scoreScreen.SetActive(true);
        m_scorePanel.SetActive(false);
    }

    //menu screen start
    public void StartButtonClicked()
    {
        m_mainMenuScreen.SetActive(false);
        m_scorePanel.SetActive(true);
        GameController.Instance.StartClicked();
    }

    //this is clicked on score summary screen
    public void RestartClicked()
    {
        m_mainMenuScreen.SetActive(true);
        m_scoreScreen.SetActive(false);
    }

    //this is clicked while in game
    public void ExitClicked()
    {
        m_scorePanel.SetActive(false);
        m_scoreScreen.SetActive(true);
        GameController.Instance.ExitClicked();
    }

    void Update()
    {
        //check if you click on the pivots on the map
        if(GameController.Instance.IsInGame)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000, 1 << LayerMask.NameToLayer("TowersPivot")) && Input.GetMouseButtonDown(0))
            {
                //notify gameContoller as he is the one dealing with money (funds)
                GameController.Instance.TowerLocationClicked(hit.transform.gameObject.GetComponent<BaseBuildingPivotView>());
            }
        }
        
    }

    //start the buy sell menu in the right location
    public void ShowBuySellMenu(Vector3 position, bool showBuy, Action<BuySellMenuAnswers> answer)
    {
        Vector3 convertedPosition = Camera.main.WorldToScreenPoint(position);
        convertedPosition -= new Vector3((Screen.width / 2), Screen.height / 2 -80f,0 );

        m_buySellMenu.gameObject.SetActive(true);
        m_buySellMenu.ShowBuYSellMenu(convertedPosition, showBuy, answer);
    }
}
